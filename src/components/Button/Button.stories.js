import React from 'react'

import { storiesOf } from '@storybook/react'
import Button from './index'
import { text, boolean } from '@storybook/addon-knobs/react'

storiesOf('Components/Button', module).add(
    'basic Button', () => (
        <Button
            label={text('label', 'Enroll')}
            disabled={boolean('disabled', false)}
            onClick={() => alert('hello there')}
        />
    ))
