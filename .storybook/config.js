import { configure } from '@storybook/react'
import { addDecorator } from '@storybook/react'
import { withKnobs, select } from '@storybook/addon-knobs/react'
import { withInfo, setDefaults } from '@storybook/addon-info'

addDecorator(withInfo)
addDecorator(withKnobs)

setDefaults({
    header: false, // Toggles display of header with component name and description
    inline: true, // Displays info inline vs click button to view
})

// automatically import all files ending in *.stories.js
const req = require.context('../src', true, /.stories.js$/)
function loadStories() {
    req.keys().forEach(filename => req(filename))
}

configure(loadStories, module)